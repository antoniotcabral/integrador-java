package controlsys.dominio;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="HORA_TURNO")
public class HoraTurno {
	
	@Id
	@Column(name = "CD_HORA_TURNO")
	private int codigo;

	@ManyToOne
	@JoinColumn(name="CD_TURNO", nullable=false)
	private TurnoTrabalho turno;

	@Column(name = "HR_INICIO")
	@Temporal(TemporalType.TIME)
	private Calendar horaInicio;

	@Column(name = "HR_FIM")
	@Temporal(TemporalType.TIME)
	private Calendar horaFim;

	@Column(name = "NU_SEQ")
	private int ordem;

	@Column(name = "NU_DIA_SEMANA")
	private String diaSemana;

	@Column(name = "BL_TRABALHA")
	private boolean trabalha;

	@Column(name = "BL_24H")
	private boolean maior24h;

	@Column(name="DT_REGISTRO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro;

	@Column(name="DT_DESATIVACAO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDesativacao;
	
	@Column(name = "BL_ATIVO")
	private boolean ativo;

	public TurnoTrabalho getTurno() {
		return turno;
	}

	public void setTurno(TurnoTrabalho turno) {
		this.turno = turno;
	}

	public Calendar getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Calendar horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Calendar getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(Calendar horaFim) {
		this.horaFim = horaFim;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}

	public boolean isTrabalha() {
		return trabalha;
	}

	public void setTrabalha(boolean trabalha) {
		this.trabalha = trabalha;
	}

	public boolean isMaior24h() {
		return maior24h;
	}

	public void setMaior24h(boolean maior24h) {
		this.maior24h = maior24h;
	}

	public Date getDataDesativacao() {
		return dataDesativacao;
	}

	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public int getCodigo() {
		return codigo;
	}
     
     
}
