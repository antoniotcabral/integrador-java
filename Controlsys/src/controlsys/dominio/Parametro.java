package controlsys.dominio;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Entity
@Table(name="PARAMETRO")
public class Parametro {

	@Id
	@Column(name = "CD_PARAMETRO")
	private int codigo;

	@Column(name = "TX_NOME")
	private String nome;

	@Column(name = "TX_VALOR")
	private String valor;

	@Column(name = "TX_TIPO")
	private String tipo;

	@Column(name = "TX_URL")
	private String url;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getCodigo() {
		return codigo;
	}

	public static List<Parametro> obterParametros(List<String> params, EntityManager em){				

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Parametro> cq = cb.createQuery(Parametro.class);

		Root<Parametro> param = cq.from(Parametro.class);

		if(params.size() > 0)
			cq.where(param.get("nome").in(params));

		TypedQuery<Parametro> query = em.createQuery(cq);

		return query.getResultList();			
	}

	public static Parametro obterParametro(String param, EntityManager em){

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Parametro> cq = cb.createQuery(Parametro.class);

		Root<Parametro> par = cq.from(Parametro.class);

		cq.where(par.get("nome").in(param));

		TypedQuery<Parametro> query = em.createQuery(cq);

		return query.getSingleResult();			

	}
}
