package controlsys.dominio;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import controlsys.persistencia.UnitOfWork;

@Entity
@Table(name = "GRUPO_TRABALHO")
public class GrupoTrabalho {

	@Id
	@Column(name="CD_GRUPO_TRABALHO")
	private int codigo;

	@Column(name="TX_NOME")
	private String nome;

	@Column(name="BL_ATIVO")
	private boolean ativo;

	@Column(name="DT_REGISTRO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro;

	@Column(name="DT_DESATIVACAO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDesativacao;

	@JoinTable(name="GRUPO_TRABALHO_TURNO")
	@ManyToMany
	private List<TurnoTrabalho> turnos;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public Date getDataDesativacao() {
		return dataDesativacao;
	}

	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}

	public List<TurnoTrabalho> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<TurnoTrabalho> turnos) {
		this.turnos = turnos;
	}

	public int getCodigo() {
		return codigo;
	}	
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}	
	
	public static List<GrupoTrabalho> obterGruposTrabalho(){
		UnitOfWork uow = null;
		EntityManager em = null;
		
		try {
		
		uow = new UnitOfWork();
		em = uow.getEntityManager();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();		
		CriteriaQuery<GrupoTrabalho> cq = cb.createQuery(GrupoTrabalho.class);			

		Root<GrupoTrabalho> cont = cq.from(GrupoTrabalho.class); 

		cq.where(cb.isNull(cont.get("dataDesativacao")));
		//cq.where(cont.get );
		cq.orderBy();

		TypedQuery<GrupoTrabalho> query = em.createQuery(cq); 				
		
		return query.getResultList();
		
		}finally{
			uow.getEmFactory().close();
		}
	}
}
