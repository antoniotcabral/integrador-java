package controlsys.dominio;

import java.util.List;

public class ControleTarefa {
	
	private int codigo;
	
	private String ip;
	
	private int porcentagem;

	public ControleTarefa(int codigo, String ip, int porcentagem){		
		this.codigo = codigo;
		this.ip = ip;
		this.porcentagem = porcentagem;		
	}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(int porcentagem) {
		this.porcentagem = porcentagem;
	}
	
	public static ControleTarefa obterTarefa(List<ControleTarefa> tarefas, int id){
		
		for (ControleTarefa controleTarefa : tarefas) {
			if(controleTarefa != null && controleTarefa.getCodigo() == id)
				return controleTarefa;
		}
		
		return null;
	}
}
