package controlsys.dominio;

public enum TipoLeitora {
			Catraca,
	        Cancela,	        
	        CancelaComTag,
	        Porta,	        
	        PortaComTeclado
}
