package controlsys.dominio;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import br.com.trielo.MDC4com.SolicitacaoDTO;

public class Pessoa {

	private String codigo;

	private String nome;

	private String cpf;

	private String passaporte;

	private String numCracha;

	private Date dataValidade;

	private Empresa empresa;

	private GrupoTrabalho grupoTrabalho;

	private String senha;

	private int codigoPapel;	


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getPassaporte() {
		return passaporte;
	}


	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}


	public String getNumCracha() {
		return numCracha;
	}


	public void setNumCracha(String numCracha) {
		this.numCracha = numCracha;
	}


	public Date getDataValidade() {
		return dataValidade;
	}


	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}


	public Empresa getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}


	public GrupoTrabalho getGrupoTrabalho() {
		return grupoTrabalho;
	}


	public void setGrupoTrabalho(GrupoTrabalho grupoTrabalho) {
		this.grupoTrabalho = grupoTrabalho;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setCodigoPapel(int codigoPapel) { 
		this.codigoPapel = codigoPapel;		
	}

	public int getCodigoPapel(){
		return this.codigoPapel;
	}
	
	public void setSenha(String senha) {
		// TODO Auto-generated method stub
		this.senha = senha;
	}

	public String getSenha() {
		// TODO Auto-generated method stub
		return this.senha;
	}
	
	public static List<Pessoa> obterPessoas(int cdControladora, Date dataLimite, EntityManager em){

		List<Pessoa> pessoas = new LinkedList<Pessoa>();

		StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_obtem_pessoas_acesso");
		sp.registerStoredProcedureParameter("cdControladora", int.class, ParameterMode.IN);
		sp.registerStoredProcedureParameter("dataLimite", Date.class, ParameterMode.IN);
		sp.setParameter("cdControladora", cdControladora);
		sp.setParameter("dataLimite", dataLimite);
		sp.execute();
		
		List<Object[]> list = sp.<Object[]>getResultList();

		for (Object[] object : list) {
			Pessoa pessoa = new Pessoa();

			pessoa.setCodigo((String)object[0]);
			pessoa.setNome((String)object[1]);
			pessoa.setCpf((String)object[2]);
			pessoa.setPassaporte((String)object[3]);
			pessoa.setNumCracha((String)object[4]);
			pessoa.setDataValidade((Date)object[5]);

			String codigoEmpresa = (String)object[6];
			Empresa empresa = new Empresa();
			empresa.setCodigo(codigoEmpresa);			
			pessoa.setEmpresa(empresa);

			/*if(object[7] != null){
				GrupoTrabalho grupoTrabalho = new GrupoTrabalho();
				grupoTrabalho.setCodigo(Integer.parseInt(object[7].toString()));
				grupoTrabalho.setNome((String)object[8]);

				List<TurnoTrabalho> turnos = new LinkedList<TurnoTrabalho>();

				TurnoTrabalho turno = new TurnoTrabalho();
				if(object[9] != null)
					turno.setObedeceEscala((boolean)object[9]);
				else
					turno.setObedeceEscala(false);
				
				turnos.add(turno);

				grupoTrabalho.setTurnos(turnos);

				pessoa.setGrupoTrabalho(grupoTrabalho);			
			}*/

			if(object[10] != null)				
				pessoa.setSenha(object[10].toString());

			pessoa.setCodigoPapel(Integer.parseInt(object[11].toString()));

			pessoas.add(pessoa);
		}

		return pessoas;		
	}	

/*	private Controladora obterControladora(List<Controladora> controls, int cdControladora)
	{
		for (Controladora controladora : controls) {
			if(controladora.getCodigo() == cdControladora)
				return controladora;			
		}

		return null;
	} */
	
	public static void emitirBaixaCracha(SolicitacaoDTO solicitacaoDTO, EntityManager em) {
		StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_emite_baixa");			
		sp.registerStoredProcedureParameter("numeroCracha", String.class, ParameterMode.IN);
		sp.setParameter("numeroCracha", solicitacaoDTO.getCodigo());
		sp.execute();
	}
}
