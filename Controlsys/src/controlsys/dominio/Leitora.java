package controlsys.dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Entity
@Table(name="LEITORA")
public class Leitora {

	@Id
	@Column(name="CD_LEITORA")
	private int codigo;
	
	@Column(name="TX_SERIE")
	private String idLeitora;

	@Column(name="TX_DIRECAO")
	private String direcaoLeitora;	
	
	@Column(name="NU_ORDEM")
	private int ordem;	

	@Column(name="BL_BAIXA_CRACHA")
	private boolean emiteBaixaCracha;
	
	@Column(name="TX_LOCAL")
	private String local;

	@Column(name="BL_SENHA")
    private boolean senha;
	
	@Column(name="BL_ATIVO")
	private boolean ativo;
	
	@Column(name="DT_REGISTRO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro;
	
	@Column(name="DT_DESATIVACAO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDesativacao;
	
	@Column(name="NU_FABRICANTE")
	private int fabricante;
	
	@ManyToOne
    @JoinColumn(name="CD_CONTROLADORA", nullable=false)
	private Controladora controladora;
        
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public Controladora getControladora() {
		return controladora;
	}

	public void setControladora(Controladora controladora) {
		this.controladora = controladora;
	}
	
	public String getIdLeitora() {
		return idLeitora;
	}
			
	public void setIdLeitora(String idLeitora) {
		this.idLeitora = idLeitora;
	}
	
	public String getDirecaoLeitora() {
		return direcaoLeitora;
	}
		
	public void setDirecaoLeitora(String direcaoLeitora) {
		this.direcaoLeitora = direcaoLeitora;
	}
			
	public String getLocal() {
		return local;
	}
	
	public void setLocal(String local) {
		this.local = local;
	}
		
	public boolean isSenha() {
		return senha;
	}
	
	public void setSenha(boolean senha) {
		this.senha = senha;
	}
		
	public Date getDataRegistro() {
		return dataRegistro;
	}
	
	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
		
	public Date getDataDesativacao() {
		return dataDesativacao;
	}
		
	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}		
    
	public Controladora getPlaca(){
		return this.controladora;
	}
	
	public void setPlaca(Controladora controladora){
		this.controladora = controladora;	
	}   
	
	public int getFabricante(){
		return fabricante;
	}
	
	public void setFabricante(int fabricante){
		this.fabricante = fabricante;	
	}
	
	public boolean isEmiteBaixaCracha() {
		return emiteBaixaCracha;
	}

	public void setEmiteBaixaCracha(boolean emiteBaixaCracha) {
		this.emiteBaixaCracha = emiteBaixaCracha;
	}
		
	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public static Leitora obterLeitora(String ipControladora, int ordem, EntityManager em){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Leitora> cq = cb.createQuery(Leitora.class);
		
		Root<Leitora> leit = cq.from(Leitora.class);	
		Path<String> cont = leit.join("controladora").<String>get("ip");
				
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		predicates.add(cb.equal(cont , ipControladora));
		predicates.add(cb.equal(leit.get("ordem"), ordem));
		predicates.add(cb.equal(leit.get("ativo"), true));
				
		cq.where(predicates.toArray(new Predicate[]{}));
		
		TypedQuery<Leitora> query = em.createQuery(cq);							
		
		List<Leitora> leitoras = query.getResultList();
		
		if(leitoras.size() > 0)
			return leitoras.get(0);		
				
		return null;
	}
}
