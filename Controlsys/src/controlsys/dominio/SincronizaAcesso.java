package controlsys.dominio;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class SincronizaAcesso {
	private int codigo;

	private String cracha;

	private String ip;

	private int codigoGrupoTrabalho;

	private String nomeGrupoTrabalho;

	private int codigoControladora;

	private Pessoa pessoa;

	private Controladora controladora;

	private String tipoSincronizacao;

	private int tarefa;

	private int status;

	private int porcentagem;

	private Date dataSincronizacao;

	public int getTarefa() {
		return tarefa;
	}

	public void setTarefa(int tarefa) {
		this.tarefa = tarefa;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(int porcentagem) {
		this.porcentagem = porcentagem;
	}

	public Date getDataSincronizacao() {
		return dataSincronizacao;
	}

	public void setDataSincronizacao(Date dataSincronizacao) {
		this.dataSincronizacao = dataSincronizacao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getTipoSincronizacao() {
		return tipoSincronizacao;
	}

	public void setTipoSincronizacao(String tipoSincronizacao) {
		this.tipoSincronizacao = tipoSincronizacao;
	}

	public String getCracha() {
		return cracha;
	}

	public void setCracha(String cracha) {
		this.cracha = cracha;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getCodigoGrupoTrabalho() {
		return codigoGrupoTrabalho;
	}

	public void setCodigoGrupoTrabalho(int codigoGrupoTrabalho) {
		this.codigoGrupoTrabalho = codigoGrupoTrabalho;
	}

	public String getNomeGrupoTrabalho() {
		return nomeGrupoTrabalho;
	}

	public void setNomeGrupoTrabalho(String nomeGrupoTrabalho) {
		this.nomeGrupoTrabalho = nomeGrupoTrabalho;
	}

	public int getCodigoControladora() {
		return codigoControladora;
	}

	public void setCodigoControladora(int codigoControladora) {
		this.codigoControladora = codigoControladora;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Controladora getControladora() {
		return controladora;
	}

	public void setControladora(Controladora controladora) {
		this.controladora = controladora;
	}

	public static SincronizaAcesso obterSincronizacao(int codSinc, EntityManager em){

		String sql = " SELECT TOP 1 SA.CD_SINCRONIZA_ACESSO, "
				+ "			  (REPLICATE('0',14-LEN(CRA.TX_RFID)) + CRA.TX_RFID) TX_RFID, "				
				+ "			  C.TX_IP,	"			
				+ "			  SA.TX_TIPO, " 
				+ "			  C.CD_CONTROLADORA, "
				+ "			  SA.DT_INICIO	"		  
				+ " FROM SINCRONIZA_ACESSO SA  WITH (NOLOCK)"
				+ " INNER JOIN CONTROLADORA C WITH (NOLOCK) ON C.CD_CONTROLADORA = SA.CD_CONTROLADORA "
				+ " LEFT JOIN CRACHA CRA WITH (NOLOCK) ON CRA.CD_CRACHA = SA.CD_CRACHA	"
				+ " WHERE SA.CD_SINCRONIZA_ACESSO = ?";		

		SincronizaAcesso sinc = null;

		try {

			Query query = em.createNativeQuery(sql).setParameter(1, codSinc);				

			Object[] object = (Object[]) query.getSingleResult();		

			sinc = new SincronizaAcesso();

			sinc.setCodigo(Integer.parseInt(object[0].toString()));

			if(object[1] != null){

				Pessoa pessoa = new Pessoa();						
				pessoa.setNumCracha(object[1].toString());					
				sinc.setPessoa(pessoa);
			}

			Controladora controladora = new Controladora();
			controladora.setCodigo(Integer.parseInt(object[4].toString()));
			controladora.setIp(object[2].toString());			

			sinc.setControladora(controladora);
			sinc.setTipoSincronizacao(object[3].toString());		

		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return sinc;
	}

	public static List<SincronizaAcesso> obterSincronizacoes(EntityManager em){		

		/*String sql = " DECLARE @dtAtual DATETIME = (SELECT CONVERT(DATETIME, CONVERT(DATE, GETDATE()))) "
				+ " DECLARE @hrAtual TIME = (SELECT CONVERT(TIME, CONVERT(VARCHAR(6), CONVERT(TIME, GETDATE())) + '00')) "

				+ " SET @dtAtual = @dtAtual + @hrAtual "

				+ " SELECT TOP 20 SA.CD_SINCRONIZA_ACESSO, "
				+ "			  (REPLICATE('0',14-LEN(CRA.TX_RFID)) + CRA.TX_RFID) TX_RFID, "				
				+ "			  C.TX_IP,	"			
				+ "			  SA.TX_TIPO, "
				+ "			  C.CD_CONTROLADORA, "
				+ "			  SA.DT_INICIO	"		  
				+ " FROM SINCRONIZA_ACESSO SA "
				+ " INNER JOIN CONTROLADORA C ON C.CD_CONTROLADORA = SA.CD_CONTROLADORA "
				+ " LEFT JOIN CRACHA CRA ON CRA.CD_CRACHA = SA.CD_CRACHA	"		
				+ " WHERE (SA.DT_INICIO IS NULL OR SA.DT_INICIO = @dtAtual OR (DATEDIFF(MI, SA.DT_INICIO, @dtAtual) > 0 AND DATEDIFF(MI, SA.DT_INICIO, @dtAtual) <= 10)) "
				+ "  AND SA.NU_STATUS = 2 "		
				+ "	   	 AND C.BL_ONLINE = 1 "
				+ "  AND NOT EXISTS "
				+ "	  (SELECT TOP 1 1 "
				+ "	   FROM SINCRONIZA_ACESSO SA1 "
				+ "	   WHERE SA1.NU_STATUS = 1 "				
				+ "		 AND ((SA.CD_CRACHA IS NOT NULL "
				+ "			   AND SA1.CD_CRACHA IS NULL "
				+ "			   AND SA1.CD_CONTROLADORA = SA.CD_CONTROLADORA) "
				+ " 		  OR (SA.CD_CRACHA IS NULL " 
				+ "				  AND (SA1.CD_CRACHA IS NULL AND SA1.CD_CONTROLADORA = SA.CD_CONTROLADORA)) " 
				+ "			 ) "
				+ "	  ) "
				+ "ORDER BY SA.DT_REGISTRO ";*/

		String sql = "	 DECLARE @dtAtual DATETIME = (SELECT CONVERT(DATETIME, CONVERT(DATE, GETDATE()))); "
			+ "  DECLARE @hrAtual TIME = (SELECT CONVERT(TIME, CONVERT(VARCHAR(6), CONVERT(TIME, GETDATE()))  + '00')); " 
			+ "	 SET @dtAtual = @dtAtual + @hrAtual " 
			+ "	 SELECT TOP 20 SA.CD_SINCRONIZA_ACESSO, " 
			+ "				  (REPLICATE('0',14-LEN(CRA.TX_RFID)) + CRA.TX_RFID) TX_RFID, " 				
			+ "				  C.TX_IP, "				
			+ "				  SA.TX_TIPO, " 
			+ "				  C.CD_CONTROLADORA, " 
			+ "				  SA.DT_INICIO	"		  
			+ "	 FROM SINCRONIZA_ACESSO SA WITH (NOLOCK) "
			+ "	 INNER JOIN CONTROLADORA C WITH (NOLOCK) ON C.CD_CONTROLADORA = SA.CD_CONTROLADORA " 
			+ "	 LEFT JOIN CRACHA CRA WITH (NOLOCK) ON CRA.CD_CRACHA = SA.CD_CRACHA	"		
			+ "	 WHERE ((SA.DT_INICIO IS NULL AND SA.CD_CRACHA IS NOT NULL) OR SA.DT_INICIO = @dtAtual OR (DATEDIFF(MI, SA.DT_INICIO, @dtAtual) > 0 AND DATEDIFF(MI, SA.DT_INICIO, @dtAtual) <= 10)) " 
			+ "	  AND SA.NU_STATUS = 2 " 		
			+ "		   	 AND C.BL_ONLINE = 1 " 
			+ "	  AND NOT EXISTS " 
			+ "		  (SELECT TOP 1 1 " 
			+ "		   FROM SINCRONIZA_ACESSO SA1 WITH (NOLOCK) "
			+ "		   WHERE SA1.NU_STATUS = 1 " 				
			+ "			 AND ((SA.CD_CRACHA IS NOT NULL " 
			+ "				   AND SA1.CD_CRACHA IS NULL " 
			+ "				   AND SA1.CD_CONTROLADORA = SA.CD_CONTROLADORA) " 
			+ "	 		  OR (SA.CD_CRACHA IS NULL  "
			+ "					  AND (SA1.CD_CRACHA IS NULL AND SA1.CD_CONTROLADORA = SA.CD_CONTROLADORA)) "  
			+ "				 ) " 
			+ "		  ) " 
			+ "	ORDER BY SA.DT_REGISTRO ";
		
		SincronizaAcesso sinc = null;

		Query query = em.createNativeQuery(sql);		

		List<SincronizaAcesso> sincronizacoes = new LinkedList<SincronizaAcesso>();

		try {

			List<Object[]> objects = query.<Object[]>getResultList();			

			for (Object[] object : objects) {

				sinc = new SincronizaAcesso();

				sinc.setCodigo(Integer.parseInt(object[0].toString()));

				if(object[1] != null){

					Pessoa pessoa = new Pessoa();						
					pessoa.setNumCracha(object[1].toString());					
					sinc.setPessoa(pessoa);
				}

				Controladora controladora = new Controladora();
				controladora.setCodigo(Integer.parseInt(object[4].toString()));
				controladora.setIp(object[2].toString());			

				sinc.setControladora(controladora);
				sinc.setTipoSincronizacao(object[3].toString());

				sincronizacoes.add(sinc);
			}

		}catch (Exception ex ){
			if(!(ex instanceof NoResultException))						
				ex.printStackTrace();			
		}

		return sincronizacoes;
	}

	public static boolean isValida(SincronizaAcesso sincroniza, EntityManager em){

		String sql = "";

		if(sincroniza.getPessoa() != null){
			sql = "SELECT TOP 1 1 "
					+ "FROM SINCRONIZA_ACESSO SA WITH(NOLOCK) "
					+ "INNER JOIN CONTROLADORA C WITH(NOLOCK) ON C.CD_CONTROLADORA = SA.CD_CONTROLADORA "
					+ "WHERE (SA.CD_CRACHA IS NULL AND SA.CD_CONTROLADORA = ?) "
					+ "AND SA.NU_STATUS = 1 "
					+ "AND C.BL_ONLINE = 1";				
		}else{
			sql = "SELECT TOP 1 1 "
					+ "FROM SINCRONIZA_ACESSO SA WITH(NOLOCK) "
					+ "INNER JOIN CONTROLADORA C WITH(NOLOCK) ON C.CD_CONTROLADORA = SA.CD_CONTROLADORA "
					+ "WHERE (SA.CD_CRACHA IS NULL AND SA.CD_CONTROLADORA = ?) "
					+ "AND SA.NU_STATUS = 1 "
					+ "AND C.BL_ONLINE = 1 ";									
		}

		Query query = em.createNativeQuery(sql).setParameter(1, sincroniza.getControladora().getCodigo()); 

		return query.getResultList().size() == 0;							
	}

	public static void atualizar(Integer codigo, Integer porcentagem, Integer status, Date dataSincronizacao, String observacoes, EntityManager em){		

		try {

			String sql = "UPDATE SINCRONIZA_ACESSO ";

			String params = "";

			if(porcentagem != null)
				params += " NU_PORCENTAGEM = " + porcentagem.intValue();

			if(status != null){
				if(!params.isEmpty())
					params += ", ";

				params += " NU_STATUS = " + status.intValue();
			}

			if(dataSincronizacao != null){
				if(!params.isEmpty())
					params += ", ";

				params += " DT_SINCRONIZA = GETDATE() ";
			}

			if(observacoes != null){
				if(!params.isEmpty())
					params += ", ";

				params += " TX_OBSERVACOES = '" + observacoes + "'";
			}

			if (!params.isEmpty())
				params = " SET " + params;


			sql += params + " WHERE CD_SINCRONIZA_ACESSO = " + codigo;

			em.getTransaction().begin();

			Query query = em.createNativeQuery(sql);											

			query.executeUpdate();
			em.getTransaction().commit();

		}catch (Exception ex) {
			if(em.getTransaction().isActive())
				em.getTransaction().rollback();

			throw ex;
		}	
	}

	public static boolean existeSincronizacaoLista(EntityManager em) {

		String sql = "SELECT TOP 1 1 "
				+ "FROM SINCRONIZA_ACESSO SA WITH(NOLOCK) "		
				+ "WHERE SA.CD_CRACHA IS NULL "
				+ "	AND SA.NU_STATUS IN (1, 2)";		

		Query query = em.createNativeQuery(sql);

		return query.getResultList().size() > 0;						
	}
}

