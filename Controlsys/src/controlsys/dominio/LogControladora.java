package controlsys.dominio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="LOG_CONTROLADORA")
public class LogControladora {

	public LogControladora(){
		
	}
	
	public LogControladora(int codigoControladora, String status, String observacoes, Date dataHora){
		this.codigoControladora = codigoControladora;
		this.status = status;
		this.observacoes = observacoes;
		
		if (dataHora == null)
			dataHora = new Date();
		
		this.dataHora = dataHora;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CD_LOG_CONTROLADORA")
	private int codigo;

	//N�o � necess�rio at� o momento mapear como associa��o a controladora. 21-05-2015 - Th�o Porto
	@Column(name="CD_CONTROLADORA")
	private int codigoControladora;
	
	@Column(name="TX_STATUS")
	private String status;
	
	@Column(name="TX_OBS")
	private String observacoes;
	
	@Column(name="DT_REGISTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHora;	
}
