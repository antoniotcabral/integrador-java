package controlsys.dominio;

import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Entity
@Table(name="CONTROLADORA")
public class Controladora {

	public Controladora(){
		leitoras = new LinkedList<Leitora>();
	}

	@Id
	@Column(name="CD_CONTROLADORA")
	private int codigo;

	@Column(name="TX_MODELO")
	private String modelo;

	@Column(name="TX_IP")
	private String ip;	

	@Column(name="NU_TIPO_CNTRL")
	private int tipoControladora;	

	@Column(name="BL_ONLINE")
	private boolean online;

	@OneToMany(mappedBy="controladora")
	private List<Leitora> leitoras;	
	
	@Column(name="DT_DESATIVACAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDesativacao;

	@Column(name="DT_REGISTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro;

	@Column(name="DE_TENSAO")
	private float tensao;
	
	@Column(name="NU_TEMP")
	private int temperatura;

	@Column(name="DE_CORRENTE")
	private float corrente;

	@Column(name="NU_QTDMAXORD")
	private int qtdMaxListaOrdenada;

	@Column(name="NU_QTDORD")
	private int qtdListaOrdenada;

	@Column(name="NU_QTDMAXDESORD")
	private int qtdMaxListaDesordenada;

	@Column(name="NU_QTDDESORD")
	private int qtdListaDesordenada;
	
	@Column(name="NU_SERIAL")
	private int numeroSerial;
	
	@Column(name="TX_VSFIRMWARE")
	private String versaoFirmWare;
	
	@Column(name="DT_ULT_ATUALIZACAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataUltAtualizacao;
		
	@Column(name="BL_ValidaOCR")
	private boolean validaOCR;	
	
	public int getTipoControladora() {
		return tipoControladora;
	}

	public void setTipoControladora(int tipoControladora) {
		this.tipoControladora = tipoControladora;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public List<Leitora> getLeitoras() {
		return leitoras;
	}

	public void setLeitoras(List<Leitora> leitoras) {
		this.leitoras = leitoras;
	}	

	public boolean isOnline() {
		return online;
	}
	
	public boolean getValidaOCR() {
		return validaOCR;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public Date getDataDesativacao() {
		return dataDesativacao;
	}

	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}

	public int getNumeroSerial() {
		return numeroSerial;
	}

	public void setNumeroSerial(int numeroSerial) {
		this.numeroSerial = numeroSerial;
	}

	public String getVersaoFirmWare() {
		return versaoFirmWare;
	}

	public void setVersaoFirmWare(String versaoFirmWare) {
		this.versaoFirmWare = versaoFirmWare;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	
	public float getTensao() {
		return tensao;
	}

	public void setTensao(float tensao) {
		this.tensao = tensao;
	}

	public int getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(int temperatura) {
		this.temperatura = temperatura;
	}

	public float getCorrente() {
		return corrente;
	}

	public void setCorrente(float corrente) {
		this.corrente = corrente;
	}

	public int getQtdMaxListaOrdenada() {
		return qtdMaxListaOrdenada;
	}

	public void setQtdMaxListaOrdenada(int qtdMaxListaOrdenada) {
		this.qtdMaxListaOrdenada = qtdMaxListaOrdenada;
	}

	public int getQtdListaOrdenada() {
		return qtdListaOrdenada;
	}

	public void setQtdListaOrdenada(int qtdListaOrdenada) {
		this.qtdListaOrdenada = qtdListaOrdenada;
	}

	public int getQtdMaxListaDesordenada() {
		return qtdMaxListaDesordenada;
	}

	public void setQtdMaxListaDesordenada(int qtdMaxListaDesordenada) {
		this.qtdMaxListaDesordenada = qtdMaxListaDesordenada;
	}

	public int getQtdListaDesordenada() {
		return qtdListaDesordenada;
	}

	public void setQtdListaDesordenada(int qtdListaDesordenada) {
		this.qtdListaDesordenada = qtdListaDesordenada;
	}

	public Date getDataUltAtualizacao() {
		return dataUltAtualizacao;
	}

	public void setDataUltAtualizacao(Date dataUltAtualizacao) {
		this.dataUltAtualizacao = dataUltAtualizacao;
	}

	public static List<Controladora> obterControladoras(EntityManager em, boolean somenteAtivas,  Date dataLimite) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Controladora> cq = cb.createQuery(Controladora.class);			

		Root<Controladora> cont = cq.from(Controladora.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		predicates.add(cb.isNull(cont.get("dataDesativacao")));

		if(!somenteAtivas)
			predicates.add(cb.isTrue(cont.<Boolean>get("online")));

		if(dataLimite != null)
			predicates.add(cb.greaterThan(cont.<Date>get("dataRegistro"), dataLimite));		

		cq.where(predicates.toArray(new Predicate[]{}));
		cq.orderBy();

		TypedQuery<Controladora> query = em.createQuery(cq); 				

		return query.getResultList();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;		
	}

	public List<Leitora> obterLeitoras(boolean ativo){

		List<Leitora> leits = new LinkedList<Leitora>();

		for (Leitora leitora : this.leitoras) {
			if(!ativo || leitora.isAtivo())
				leits.add(leitora);
		}

		return leits;
	}

	public static Controladora obterPorIP(String ipUnidadeRemota, EntityManager em) {		
		
		CriteriaBuilder cb = em.getCriteriaBuilder();		
		CriteriaQuery<Controladora> cq = cb.createQuery(Controladora.class);			

		Root<Controladora> cont = cq.from(Controladora.class); 

		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(cb.equal(cont.get("ip") , ipUnidadeRemota));
		predicates.add(cb.isNull(cont.get("dataDesativacao")));

		cq.where(predicates.toArray(new Predicate[]{}));				

		TypedQuery<Controladora> query = em.createQuery(cq); 				

		return query.getSingleResult();
		
	}	
	
	public static int tornarControladoraOff (int codSincronizacao, EntityManager em){					
		//teste
		String sql = " UPDATE CONTROLADORA SET BL_ONLINE = 0, DT_ULT_ATUALIZACAO = GETDATE()"
				+    " WHERE CD_CONTROLADORA = (SELECT CD_CONTROLADORA "
								  + " FROM SINCRONIZA_ACESSO SA WITH(NOLOCK) "
								  + " WHERE SA.CD_SINCRONIZA_ACESSO = ?)";					

		Query query = em.createNativeQuery(sql);				
		query.setParameter(1, codSincronizacao);			
		
		return query.executeUpdate();		
	}
}
