package controlsys.dominio;

public class StatusLogControladora {

	public static String Online = "Online";
	public static String Offline = "Offline";
	public static String Liberada = "Liberado";
	public static String Interditada = "Interditado";
	public static String Ativada = "Ativo";
	public static String Inativada = "Inativo";
}
