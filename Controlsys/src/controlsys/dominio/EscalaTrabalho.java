package controlsys.dominio;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Entity
@Table(name="ESCALA")
public class EscalaTrabalho {
	
	@Id
	@Column(name="CD_ESCALA")
	private int codigo;

	@Column(name="DT_ESCALA", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEscala;

	@Column(name="DT_REGISTRO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro;

	@ManyToOne
	@JoinColumn(name="CD_GRUPO_TRABALHO", nullable=false)
	private GrupoTrabalho grupoTrabalho;

	@ManyToOne
	@JoinColumn(name="CD_HORA_TURNO", nullable=false)
	private HoraTurno horaTurno;     

    public Date getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	
	public Date getDataEscala() {
		return dataEscala;
	}

	public void setDataEscala(Date dataEscala) {
		this.dataEscala = dataEscala;
	}

	public GrupoTrabalho getGrupoTrabalho() {
		return grupoTrabalho;
	}

	public void setGrupoTrabalho(GrupoTrabalho grupoTrabalho) {
		this.grupoTrabalho = grupoTrabalho;
	}

	public HoraTurno getHoraTurno() {
		return horaTurno;
	}

	public void setHoraTurno(HoraTurno horaTurno) {
		this.horaTurno = horaTurno;
	}

	public int getCodigo() {
		return codigo;
	}

	public static List<EscalaTrabalho> obterEscalas(Date dateMin, Date dateMax, EntityManager em) {						
		
		CriteriaBuilder cb = em.getCriteriaBuilder();		
		CriteriaQuery<EscalaTrabalho> cq = cb.createQuery(EscalaTrabalho.class);			

		Root<EscalaTrabalho> cont = cq.from(EscalaTrabalho.class); 

		cq.where(cb.greaterThan(cont.<Date>get("dataEscala"), dateMin));
		cq.where(cb.lessThan(cont.<Date>get("dataEscala"), dateMax));
		cq.orderBy();

		TypedQuery<EscalaTrabalho> query = em.createQuery(cq); 				
		
		return query.getResultList();			
	}
	
	public static List<HoraTurno> obterHorasTurno(List<EscalaTrabalho> escalas) {
		List<HoraTurno> horas = new LinkedList<HoraTurno>();
		
		for (EscalaTrabalho escala : escalas) {
			if (!existeHoraTurno(horas, escala.getHoraTurno())) {
				horas.add(escala.getHoraTurno());
			}
		}
		
		return horas;
	}
	
	public static boolean existeHoraTurno(List<HoraTurno> horas, HoraTurno hora){
		for (HoraTurno horaTurno : horas) {
			if(horaTurno.getCodigo() == hora.getCodigo())
				return true;
		}
		
		return false;
	}

	public static List<GrupoTrabalho> obterGruposTrabalho(List<EscalaTrabalho> escalas) {
		List<GrupoTrabalho> gruposTrabalho = new LinkedList<GrupoTrabalho>();

		for (EscalaTrabalho escala : escalas) {
			if (!existeGrupoTrabalho(gruposTrabalho, escala.getGrupoTrabalho()))
				gruposTrabalho.add(escala.getGrupoTrabalho());			
		}

		return gruposTrabalho;
	}
	
	public static boolean existeGrupoTrabalho(List<GrupoTrabalho> grupos, GrupoTrabalho grupoTrabalho){
		for (GrupoTrabalho grupo : grupos) {
			if(grupo.getCodigo() == grupoTrabalho.getCodigo())
				return true;
		}
		
		return false;
	}

	public static List<EscalaTrabalho> obterEscalasPorGrupo(List<EscalaTrabalho> escalas, GrupoTrabalho grupoTrabalho) {
		List<EscalaTrabalho> escalasGrupo = new LinkedList<EscalaTrabalho>();
		
		for (EscalaTrabalho escalaTrabalho : escalas) {
			if(escalaTrabalho.getGrupoTrabalho().getCodigo() == grupoTrabalho.getCodigo())
				escalasGrupo.add(escalaTrabalho);
		}
		
		return escalasGrupo;
	}
}
