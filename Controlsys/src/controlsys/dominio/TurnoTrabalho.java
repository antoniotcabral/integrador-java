package controlsys.dominio;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TURNO")
public class TurnoTrabalho {
	
	@Id
	@Column(name = "CD_TURNO")
	private int codigo;

	@Column(name = "TX_NOME")
	private String nome;

	@Column(name = "BL_ATIVO")
	private boolean ativo;

	@Column(name="DT_REGISTRO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRegistro;
	
	@Column(name = "BL_FERIADO")
	private boolean consideraFeriado;

	@Column(name = "BL_ESCALA")
	private boolean obedeceEscala;

	@OneToMany(mappedBy="turno")
	private List<HoraTurno> horasTurno;

	@Column(name="DT_DESATIVACAO", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataDesativacao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public boolean isConsideraFeriado() {
		return consideraFeriado;
	}

	public void setConsideraFeriado(boolean consideraFeriado) {
		this.consideraFeriado = consideraFeriado;
	}

	public boolean isObedeceEscala() {
		return obedeceEscala;
	}

	public void setObedeceEscala(boolean obedeceEscala) {
		this.obedeceEscala = obedeceEscala;
	}

	public List<HoraTurno> getHorasTurno() {
		return horasTurno;
	}

	public void setHorasTurno(List<HoraTurno> horasTurno) {
		this.horasTurno = horasTurno;
	}

	public Date getDataDesativacao() {
		return dataDesativacao;
	}

	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}	

	public int getCodigo() {
		return codigo;
	}
	
	
}
