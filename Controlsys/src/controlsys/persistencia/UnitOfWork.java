package controlsys.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UnitOfWork {
	private EntityManagerFactory emFactory;
	private EntityManager em;
	
	public EntityManagerFactory getEmFactory(){
		if(emFactory == null){
			emFactory = Persistence.createEntityManagerFactory("conexao");
		}
		
		return emFactory;
	}
	
	public EntityManager getEntityManager(){
		EntityManagerFactory emFact = getEmFactory();
					
		if(em == null)
		   em = emFact.createEntityManager();
		
		return em;
	}
}
