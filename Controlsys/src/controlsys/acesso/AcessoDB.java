package controlsys.acesso;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;

import controlsys.dominio.*;
import controlsys.persistencia.UnitOfWork;
import br.com.trielo.MDC4com.*;

public class AcessoDB implements InterfaceDB {		

	private final int CONST_TRF_AUXILIARES = 2147483547;

	/*********** Definicoes de tarefas *************************************************************************************/
	//	private final int TRF_SEM_TAREFA                    =   0;
	private final int TRF_DATAHORA                      =   1;
	//	private final int TRF_ADD_LISTA_FAIXAHORARIA        =   2;
	//  private final int TRF_ADD_FAIXAHORARIA              =   3;
	//	private final int TRF_DEL_FAIXAHORARIA              =   4;
	//	private final int TRF_ADD_LISTA_ZONATEMPO           =   5;
	//	private final int TRF_ADD_ZONATEMPO                 =   6;
	//	private final int TRF_DEL_ZONATEMPO                 =   7;
	private final int TRF_ADD_LISTA_INPUT               =   8;
	//	private final int TRF_ADD_INPUT                     =   9;
	//	private final int TRF_DEL_INPUT                     =   10;
	private final int TRF_ADD_LISTA_ACIONAMENTO         =   11;
	//	private final int TRF_ADD_ACIONAMENTO               =   12;
	//	private final int TRF_DEL_ACIONAMENTO               =   13;
	private final int TRF_ADD_LISTA_MOD_ACI             =   14;
	//	private final int TRF_ADD_MOD_ACI                   =   15;
	//	private final int TRF_DEL_MOD_ACI                   =   16;
	private final int TRF_ADD_LISTA_USUARIO             =   17;
	private final int TRF_ADD_USUARIO                   =   18;
	private final int TRF_DEL_USUARIO                   =   19;
	//	private final int TRF_CAP_TEMPLATE_HK               =   20;
	//	private final int TRF_CAP_TEMPLATE_FINGER           =   21;
	//	private final int TRF_ADD_LISTA_TEMPLATE_FINGER     =   22;
	//	private final int TRF_ADD_TEMPLATE_FINGER           =   23;
	//	private final int TRF_DEL_TEMPLATE_FINGER           =   24;

	/*********** Definicoes de ocorrencias *********************************************************************************/
	/*	private final int OCO_UR_LEITURA_CARTAO             =   0;
	private final int OCO_UR_ACESSO_LIBERADO            =   1;
	private final int OCO_UR_NAO_CADASTRADO             =   2;
	private final int OCO_UR_EXCLUIDO                   =   3;
	private final int OCO_UR_AGUARDANDO_INPUT2          =   4;
	private final int OCO_UR_HORARIO_INVALIDO           =   5;
	private final int OCO_UR_VIOLACAO_DE_FLUXO          =   6;
	private final int OCO_UR_LEITORA_INVALIDA           =   7;
	private final int OCO_UR_NEGADO_INPUT2              =   8;
	private final int OCO_UR_NEGADO_LISTA_NEGRA         =   9;
	private final int OCO_UR_ENTRADA_COMPLETA           =   10;
	private final int OCO_UR_SAIDA_COMPLETA             =   11;
	private final int OCO_UR_ACESSO_NAO_CONCLUIDO       =   12;
	private final int OCO_UR_TIMEOUT_INPUT2				=   13;
	private final int Acionamento inv�lido				=   14;
	private final int Modo de libera��o inv�lido		=   15;
	private final int OCO_UR_PONTO_REGISTRADO           =   20;
	private final int OCO_UR_TIMEOUT_COMUNICACAO_HK     =   21;
	private final int OCO_UR_TIMEOUT_LEITURA_BIO        =   22;
	private final int OCO_UR_MAO_INVALIDA               =   23;
	private final int OCO_UR_DIGITAL_INVALIDA           =   24;
	private final int OCO_UR_ALARME_ATIVADO             =   30;
	private final int OCO_UR_ALARME_DESATIVADO          =   31;
	private final int OCO_UR_ALARME_PORTA_ABERTA        =   32;
	private final int OCO_UR_ALARME_INTRUSAO            =   33;
	private final int OCO_UR_RESET1                     =   40;
	private final int OCO_UR_RESET2                     =   41;
	private final int OCO_UR_RESET3                     =   42;
	private final int OCO_UR_RESET4                     =   43;
	private final int OCO_UR_RESET5                     =   44;
	private final int OCO_UR_RESET6                     =   45;
	private final int OCO_UR_RESET7                     =   46;
	private final int OCO_UR_MAX_TENSAO                 =   50;
	private final int OCO_UR_MAX_CORRENTE               =   51;
	private final int OCO_UR_MAX_TEMPERATURA            =   52;
	private final int OCO_UR_NRM_TENSAO                 =   53;
	private final int OCO_UR_NRM_CORRENTE               =   54;
	private final int OCO_UR_NRM_TEMPERATURA            =   55;
	private final int OCO_UR_MIN_TENSAO                 =   56;
	private final int OCO_UR_MIN_TEMPERATURA            =   57; */
	private final int OCO_UR_ONLINE                     =   300;
	private final int OCO_UR_OFFLINE                    =   301;
private boolean veiculoPermissao = true;
	/*	private final int OCO_UR_DEMITIDO                   =   81;
	private final int OCO_UR_INATIVO                    =   82;
	private final int OCO_UR_VISTO_EXPIRADO  			=   83;
	private final int OCO_UR_VISITANTE_EXPIRADO 		=   84;
	private final int OCO_UR_PRESTADOR_EXPIRADO 		=   85;
	private final int OCO_PED_EMPRESA_VENCIDO 			=   86;
	private final int OCO_UR_CNH_VENCIDA				=   87;
	private final int OCO_UR_TREINAMENTO			 	=   88;
	private final int OCO_UR_SENHA_INVALIDA				=   89;
	private final int OCO_UR_ANTIPASSBACK 				=   90;
	private final int OCO_UR_SUPERVISIONADO				=   91;  */
	private final int OCO_UR_BAIXA_CRACHA				=	92;	

	private List<ControleTarefa> tarefas = new LinkedList<ControleTarefa>();

	/********************************************************************************************************************************
	 * Retorna configuracaoes das placas
	 *
	 * @param concentrador a funcao retorna apenas as configuracoes das placas cadastradas para este concentrador.
	 * @param ipUnidadeRemota caso o valor for null retornar o vetor de UnidadeRemotaDTO com as configura��eses de todas as Unidades Remotas
	 * cadastradas para o concentrador. Caso o valor n�o seja null, retornar o vetor de UnidadeRemotaDTO com apenas a
	 * configura��o da placa que possui o mesmo ip. EX:"10.1.1.89"
	 * @return vetor de UnidadeRemotaDTO
	 ********************************************************************************************************************************/

	public Vector<UnidadeRemotaDTO> getUnidadeRemotaDTO(int concentrador, String ipUnidadeRemota) {
		System.out.println(" ---------------------- Envia Lista de Unidade Remota ---------------------- ");					

		Vector<UnidadeRemotaDTO> listaConfigPlacas = new Vector<UnidadeRemotaDTO>();		

		/********************************************************************************

		TIPO DE LEITORA CONFIGURADO:
			WIEGAND								= 1
	 		CLOCK&DATA | MAGSTRIPE | ABATRAK 	= 2
			MIFARE-ACURA						= 3
			MIFARE-INELTEC						= 4
			CODIGO DE BARRAS COMPEX				= 5

		TIPO DE TECLADO CONFIGURADO:
			M�DULO DE TECLADO TRIELO			= 1
			TECLADO HANDKEY						= 2

		 ********************************************************************************/

		EntityManager em = null;

		try{

			em = new UnitOfWork().getEntityManager();

			List<String> params = new LinkedList<String>();

			params.add("TempoEsperaInput2");
			params.add("TempoRepeteInput1");
			params.add("IntervaloKeepOn");
			params.add("IntervaloKeepOff");
			params.add("TempoGiro");
			params.add("PortaEntrada");
			params.add("IntervaloRepeteInput1");

			List<Parametro> parametros = Parametro.obterParametros(params, em);			

			int portaEntrada = Integer.parseInt(obterParametro(parametros, "PortaEntrada"));
			int tempoEsperaInput2 = Integer.parseInt(obterParametro(parametros, "TempoEsperaInput2"));
			int tempoRepeteInput1 = Integer.parseInt(obterParametro(parametros, "TempoRepeteInput1"));
			int intervaloRepeteInput1 = Integer.parseInt(obterParametro(parametros, "IntervaloRepeteInput1"));
			int tempoGiro = Integer.parseInt(obterParametro(parametros, "TempoGiro"));
			int intervaloKeepOn = Integer.parseInt(obterParametro(parametros, "IntervaloKeepOn"));
			int intervaloKeepOff = Integer.parseInt(obterParametro(parametros, "IntervaloKeepOff"));					

			List<Controladora> controladoras = null;

			if(ipUnidadeRemota == null)
				controladoras = Controladora.obterControladoras(em, true, null);
			else {
				controladoras = new LinkedList<Controladora>();
				controladoras.add(Controladora.obterPorIP(ipUnidadeRemota, em));
			}

			for (Controladora controladora : controladoras) {
				UnidadeRemotaDTO temp = obterUnidadeRemota(portaEntrada, tempoEsperaInput2,
						tempoRepeteInput1, intervaloRepeteInput1,
						tempoGiro, intervaloKeepOn, intervaloKeepOff,
						controladora);

				listaConfigPlacas.add(temp);
			}

		}
		catch(Exception ex){
			ex.printStackTrace();
		}		
		finally {
			if(em != null)							
				em.getEntityManagerFactory().close();
			//em.close();			
		}

		return listaConfigPlacas;
	}

	public PermissaoDTO validaCodigo(SolicitacaoDTO solicitacaoDTO) {

		int ocorrencia = 2;
		boolean temPermissao = false;
		char sentido = 'E';
		boolean controlaFluxo = false; //A libera��o foi com fluxo o ativado ou n�o.

		EntityManagerFactory emFactory = null;
		EntityManager em = null;

		try{				
			System.out.println("<------------ Efetua valida��o de acesso em :" + solicitacaoDTO.getCodigo() + "------------------------>");
			System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));

			emFactory = Persistence.createEntityManagerFactory("conexao");															
			em = emFactory.createEntityManager();

			Controladora controladora = Controladora.obterPorIP(solicitacaoDTO.getIpUnidadeRemota(), em);				

			if(!controladora.isOnline()){
				System.out.println(" ---------------> teste: est� offline <--- " + solicitacaoDTO.getIpUnidadeRemota());
				System.out.println(" ---------------> in�cio update online <--- " + solicitacaoDTO.getIpUnidadeRemota());
				System.out.println(new Date().toString());
				em.getTransaction().begin();								
				controladora.setOnline(true);
				em.getTransaction().commit();
				System.out.println(" ---------------> fim update online <--- " + solicitacaoDTO.getIpUnidadeRemota());
				System.out.println(new Date().toString());
			}

			Leitora leitora = Leitora.obterLeitora(solicitacaoDTO.getIpUnidadeRemota(), solicitacaoDTO.getNumeroDispositivo(), em);
			
			if(leitora.isEmiteBaixaCracha()) {
				try {
					Pessoa.emitirBaixaCracha(solicitacaoDTO, em);
				}catch (Exception e){

				}
				return new PermissaoDTO(false, 'E', OCO_UR_BAIXA_CRACHA, false);
			}

			StoredProcedureQuery sp = null;	
			sp = em.createStoredProcedureQuery("sp_valida_acesso");

			sp.registerStoredProcedureParameter("numeroCracha", String.class, ParameterMode.IN);
			sp.registerStoredProcedureParameter("numLeitora", int.class, ParameterMode.IN);		
			sp.registerStoredProcedureParameter("senha", String.class, ParameterMode.IN);
			sp.registerStoredProcedureParameter("ipControladora", String.class, ParameterMode.IN);

			sp.setParameter("numeroCracha", solicitacaoDTO.getCodigo());
			sp.setParameter("numLeitora", solicitacaoDTO.getNumeroDispositivo());
			sp.setParameter("senha", null);
			sp.setParameter("ipControladora", solicitacaoDTO.getIpUnidadeRemota());		

			sp.execute();

			Object[] a = (Object[])sp.getResultList().get(0);

			
			ocorrencia=(int)a[0];
			temPermissao = (boolean)a[1];
						
			int valorOcorrenciaOriginal = (int)a[0];
			boolean temPermissaoOriginal = (boolean)a[1];
			
			
			try {
				//Verifica se a controladora � uma Cancela
				if(controladora.getTipoControladora() == 3 && controladora.getValidaOCR() == true) {
					
					 temPermissao = false;
					  ocorrencia = 25;
					List<String> params = new LinkedList<String>();
					params.add("IP_INTEGRADOR");
					params.add("PORTA_INTEGRADOR_AVIGILON");
					params.add("ATIVACAO_CONSULTA_INTEGRADOR");
					params.add("ESPERA_AVIGILON");
					List<Parametro> parametros = Parametro.obterParametros(params, em);
					
				
		        			        	
					String IP_INTEGRADOR = String.valueOf(obterParametro(parametros, "IP_INTEGRADOR"));
					String PORTA_INTEGRADOR_AVIGILON = String.valueOf(obterParametro(parametros, "PORTA_INTEGRADOR_AVIGILON"));
					String ATIVACAO_CONSULTA_INTEGRADOR = String.valueOf(obterParametro(parametros, "ATIVACAO_CONSULTA_INTEGRADOR"));
					
					String ESPERA_AVIGILON = String.valueOf(obterParametro(parametros, "ESPERA_AVIGILON"));					
					String espera = ESPERA_AVIGILON.replaceAll(",", ".");
					float esperaAvigilon = Float.parseFloat(espera) * 1000;					
					Integer esperaAvg = (int)esperaAvigilon;					
					//int esperaAvg = Integer.parseInt(String.valueOf(esperaAvigilon));
					
					//Intelig�ncia de ativa e desativar a valida��o por ve�culo
					if(ATIVACAO_CONSULTA_INTEGRADOR.equalsIgnoreCase("Sim")) {
						 
						  long tempoInicio = System.nanoTime();
			        	  
					        	 String urlTeste =  IP_INTEGRADOR + ":"+ PORTA_INTEGRADOR_AVIGILON + "/api/integracao/ObterPermissaoVeiculo?IdLeitora=" + leitora.getCodigo()  + "&IpControladora="+ controladora.getCodigo() +"&numCracha=" + solicitacaoDTO.getCodigo() + "&papelPermissao=" + temPermissaoOriginal;
					        	 					        	  
					        	  URL url = new URL(urlTeste);
					 	         //URL url = new URL(IP_INTEGRADOR + ":" + PORTA_INTEGRADOR_AVIGILON + "/api/integracao/ObterPermissaoVeiculo?IdLeitora=" + leitora.getCodigo()  + "&IpControladora="+ controladora.getCodigo() +"&numCracha=" + solicitacaoDTO.getCodigo() + "&papelPermissao=" + temPermissao);
					 				    //URL url = new URL("http://192.168.100.110:38880/api/integracao/ObterPermissaoVeiculo?IdLeitora=" + 57  + "&IpControladora="+ 106 +"&numCracha=" + 3543793+ "&papelPermissao=" + temPermissao);
					 				   HttpURLConnection conn = (HttpURLConnection) url.openConnection();					 				   
					 				
					 				   conn.setRequestMethod("GET");
					 				   conn.setRequestProperty("Accept", "application/json");
					 				   conn.setConnectTimeout(esperaAvg);
					 				  Boolean valida = false;
					 				 long tempoPassado  = System.nanoTime() - tempoInicio;
				 			         String output;
				 			        long durationInMs = TimeUnit.MILLISECONDS.convert(tempoPassado, TimeUnit.NANOSECONDS);
				 			        
				 			        if (durationInMs < esperaAvg) {
				 			        	if (conn.getInputStream() != null) {

							 			    InputStreamReader in = new InputStreamReader(conn.getInputStream());
							 			    BufferedReader br = new BufferedReader(in);
							 			         
						 			        while ((output = br.readLine()) != null) {
						 			        	 if(output.contains("true") || output.contains("True") ) {
								 			        	valida = true;
								 			         }					            
						 			        }
						 			    	if(temPermissaoOriginal == true) {
							 			    		if(valida == false) 
						 			        	 {
						 			        		temPermissao = false;
						 			        		ocorrencia = 25;
						 			        	 }
							 			    		
							 			    		if(valida == true) 
							 			        	 {
							 			        		temPermissao = true;
							 			        		ocorrencia =1;
							 			        	 }
						 			        }
						 			    	else
						 			    	{
						 			    		  if(valida == true) {
							 			        		temPermissao = false;
							 			        		ocorrencia = 26;
							 			             }else {
							 			            	temPermissao = false;
							 			        		ocorrencia = 27;
							 			             }	
						 			        }

				 			        	}
				 			        }
						 			    
				 			         
				 			
				 			 conn.disconnect();
				 			}
				 				
					
			        
				}
			}
			catch(Exception e) {
				 temPermissao = false;
	        	  ocorrencia = 27; 
			}
			
			
			
			sentido = ((String)a[2]).charAt(0);						

			System.out.println("<------------ Fim valida��o de acesso em :" + solicitacaoDTO.getCodigo() + "------------------------>");
			System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));			
		}catch (Exception ex){
			if(em != null){
				if(em.getTransaction().isActive()){
					em.getTransaction().rollback();
					System.out.println(new Date().toString());					
				}
			}
		}
		finally {
			if(emFactory != null)								
				emFactory.close();						
		}
		
		int idDisplay=1;		// Padr�o 1 , utilizar valor 21 para display de handkey
		String mensagemDisplay= " MENSAGEM TESTE "; // Mensagem sempre com 16 caracteres
		int tempoMensagem=3;	// tempo em segundos em que a mensagem � exibida no display
		int tempoLed1=1;		// tempo em segundos do led1
		int tempoLed2=0;		// tempo em segundos do led2
		int tempoBuzzer=0;		// tempo em segundos do buzzer		

		//return new PermissaoDTO(temPermissao,sentido,ocorrencia,controlaFluxo);	// Construtor v�lido, utiliza padr�o de mensagens e acionamentos.				
		return new PermissaoDTO(temPermissao,sentido,ocorrencia,controlaFluxo,idDisplay,mensagemDisplay,tempoMensagem,tempoLed1,tempoLed2,tempoBuzzer);
	}

	public Vector<TarefaDTO> getTarefaDTO(int concentrador) {
		System.out.println(" ---------------------- Verifica Lista de Tarefas ---------------------- ");				

		Vector<TarefaDTO> listaTarefas = new Vector<TarefaDTO>();
		//Vector<RegistroDTO> registros = new Vector<RegistroDTO>();

		EntityManager em = null;

		try {

			em = new UnitOfWork().getEntityManager();

			//Parametro paramCatracas = Parametro.obterParametro("CatracasAtualizadas", em);			
			//Parametro paramDataUltIncremento = Parametro.obterParametro("DataUltimoIncremento", em);			
			//Date dataUltIncremento = convertDate(paramDataUltIncremento.getValor());

			Parametro paramDataUlt = Parametro.obterParametro("DataUltimaAtualizacaoEscala", em);					
			Date dataUltimaAtualizacao = convertDate(paramDataUlt.getValor());

			Calendar calendar = Calendar.getInstance(); 
			calendar.setTime(dataUltimaAtualizacao);		
			calendar.add(Calendar.DAY_OF_MONTH, -7);

			//List<EscalaTrabalho> escalas = EscalaTrabalho.obterEscalas(calendar.getTime(), dataUltimaAtualizacao, em);																		

			//Vector<RegistroDTO> faixasHorarias = obterFaixasHorarias(escalas);
			//Vector<RegistroDTO> zonasTempo = obterZonasTempo(escalas);
			Vector<RegistroDTO> acionamentos = obterConfiguracaoAcionamentos();
			Vector<RegistroDTO> modosAcionamentos = obterModosAcionamentos();


			System.out.println(" ---------------------- Verifica sicroniza��es ---------------------- ");
			System.out.println(new Date().toString());
			List<SincronizaAcesso> sincronizaAcessos = SincronizaAcesso.obterSincronizacoes(em);


			System.out.println(" ---------------------- Insere sincroniza��es ---------------------- ");
			System.out.println(new Date().toString());
			for (SincronizaAcesso sincronizaAcesso : sincronizaAcessos) {

				Vector<RegistroDTO> usuarios = new Vector<RegistroDTO>();

				Controladora controladora = Controladora.obterPorIP(sincronizaAcesso.getControladora().getIp(), em);
				int tarefa = 0;			

				if (controladora.isOnline() && SincronizaAcesso.isValida(sincronizaAcesso, em)) {

					if(sincronizaAcesso.getPessoa() != null){
						usuarios.add(criarUsuario(sincronizaAcesso.getPessoa()));

						tarefa = TRF_ADD_USUARIO;

						if(sincronizaAcesso.getTipoSincronizacao().equals("Suspender"))
							tarefa = TRF_DEL_USUARIO;						

						System.out.println(" --- add usu�rio status 1 ---- cod " + sincronizaAcesso.getCodigo() + "-- rfid --" + sincronizaAcesso.getPessoa().getNumCracha()   +  "---tar --" + tarefa + "---ip----" + controladora.getIp());
						listaTarefas.add(new TarefaDTO(sincronizaAcesso.getCodigo(), tarefa, usuarios,  controladora.getIp()));

						System.out.println(" ---------------------- Atualiza sinc usu status 1 ---- cod " + sincronizaAcesso.getCodigo() + "-----");
						System.out.println(new Date().toString());
						SincronizaAcesso.atualizar(sincronizaAcesso.getCodigo(), 0, 1, null, null, em);															
					}else{
						usuarios = obterUsuarios(controladora, null, em);

						if(usuarios.size() > 0) {

							int contTarefa = CONST_TRF_AUXILIARES;

							/*********** Envio de Faixa Horaria ************************************************************************************//*
							listaTarefas.add(new TarefaDTO(++contTarefa, TRF_ADD_LISTA_FAIXAHORARIA, faixasHorarias, ip));

	  						/*********** Envio de Zona de Tempo ************************************************************************************//*
							listaTarefas.add(new TarefaDTO(++contTarefa,TRF_ADD_LISTA_ZONATEMPO, zonasTempo, ip));

	   						/*********** Envio de Input Habilitado *********************************************************************************/															
							listaTarefas.add(new TarefaDTO(++contTarefa,TRF_ADD_LISTA_INPUT, obterConfiguracoesDeInput(controladora), controladora.getIp()));

							/*********** Envio de Acionamento **************************************************************************************/						   		          
							listaTarefas.add(new TarefaDTO(++contTarefa,TRF_ADD_LISTA_ACIONAMENTO, acionamentos, controladora.getIp()));

							/*********** Envio de modos de Acionamento *****************************************************************************/
							listaTarefas.add(new TarefaDTO(++contTarefa, TRF_ADD_LISTA_MOD_ACI, modosAcionamentos, controladora.getIp()));

							tarefa = TRF_ADD_LISTA_USUARIO;

							listaTarefas.add(new TarefaDTO(sincronizaAcesso.getCodigo(), tarefa, usuarios, controladora.getIp()));							

							System.out.println(" ---------------------- Atualiza sinc list status 1 ---------------------- ");
							System.out.println(new Date().toString());
							SincronizaAcesso.atualizar(sincronizaAcesso.getCodigo(), 0, 1, null, null, em);

							tarefas.add(new ControleTarefa(sincronizaAcesso.getCodigo(), sincronizaAcesso.getIp(), 0));							
						}
						else{
							System.out.println(" ---------------------- Atualiza sinc status 3 ---------------------- ");
							System.out.println(new Date().toString());
							SincronizaAcesso.atualizar(sincronizaAcesso.getCodigo(), 100, 3, new Date(), "A controladora n�o possuia usu�rios no momento da sincroniza��o", em);
						}
					}					
				}
			}			

			System.out.println(" ---------------------- Retorna lista ---------------------- ");
			System.out.println(new Date().toString());
			return listaTarefas;

		} catch (Exception e) {
			if(em.getTransaction().isActive())
				em.getTransaction().rollback();			

			e.printStackTrace();
		}finally{
			if(em != null)
				em.getEntityManagerFactory().close();				
		}

		return listaTarefas;
	}	

	public void progressoTarefa(long id, int porcentagem, int ocorrencia) {		
		System.out.println(" -> Progresso Tarefa: "+ id + " Porcentagem concluida: "+ porcentagem +"%  Ocorrencia: "+ ocorrencia);

		if (id > CONST_TRF_AUXILIARES)
			return;		

		EntityManager em = null;

		try {

			Integer status = null;
			Integer percentual = porcentagem;
			String obs = null;
			Date dataSincronizacao = null;

			switch (ocorrencia) {
			case 1: //ONLINE
				status = 1;				
				if(porcentagem == 100){					
					em = new UnitOfWork().getEntityManager();

					System.out.println(" ---------------> Concluindo sincroniza��o " + id + " <--- ");
					status = 3;
					dataSincronizacao = new Date();							

					Parametro paramCatracas = Parametro.obterParametro("CatracasAtualizadas", em);

					em.getTransaction().begin();

					if(SincronizaAcesso.existeSincronizacaoLista(em))
						paramCatracas.setValor("Atualizando");
					else
						paramCatracas.setValor("Sim");

					em.getTransaction().commit();

					SincronizaAcesso.atualizar((int)id, percentual, status, dataSincronizacao, obs, em);

					ControleTarefa taref = ControleTarefa.obterTarefa(tarefas, (int)id);	

					if(taref != null)
						tarefas.remove(taref);
				}
				else{

					ControleTarefa tarefa = ControleTarefa.obterTarefa(tarefas, (int)id);

					if(tarefa != null){						
						if(tarefa.getPorcentagem() != porcentagem 
								&& (porcentagem % 10) == 0){
							em = new UnitOfWork().getEntityManager();							
							tarefa.setPorcentagem(porcentagem);
							SincronizaAcesso.atualizar((int)id, percentual, status, dataSincronizacao, obs, em);
						}
					}
				}
				break;
			case 2: //ERRO
				em = new UnitOfWork().getEntityManager();
				obs = "Retransmitindo pois houve erro na sincroniza��o";
				status = 2;
				percentual = null;
				SincronizaAcesso.atualizar((int)id, percentual, status, dataSincronizacao, obs, em);
				break;
			case 3:	//OFFLINE			
				em = new UnitOfWork().getEntityManager();
				SincronizaAcesso sinc = SincronizaAcesso.obterSincronizacao((int)id, em);

				if(sinc.getPessoa() != null){
					obs = "Retransmitindo pois a placa ficou offline";
					status = 2;					
				}else {
					status = 4;
					obs = "Retransmitindo pois a placa ficou offline";
				}
				percentual = null;

				System.out.println(" ---------------> in�cio update offline <--- ");
				System.out.println(new Date().toString());

				em.getTransaction().begin();				

				int upted = Controladora.tornarControladoraOff((int)id, em);

				LogControladora logControladora = new LogControladora(sinc.getControladora().getCodigo(),
						StatusLogControladora.Offline,
						"Sincroniza��o falhou. Placa offline.", 
						new Date());
				em.persist(logControladora);

				em.getTransaction().commit();

				System.out.println(" ---------------> fim update offline <--- Atualizou " + upted);
				System.out.println(new Date().toString());				


				SincronizaAcesso.atualizar((int)id, percentual, status, dataSincronizacao, obs, em);				
				break;
			}						
		}catch (Exception e) {
			e.printStackTrace();

			if(em != null 
					&& em.getTransaction().isActive())
				em.getTransaction().rollback();

		}finally{
			if(em != null)
				em.getEntityManagerFactory().close();
		}
	}

	public void geraMarcacao(MarcacaoDTO marcacao) {
		System.out.println(" ---------------------- Trata Marcacao ---------------------- ");
		System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));

		System.out.println(" -> Concentrador:"+marcacao.getConcentrador());
		System.out.println(" -> Ip UnidadeRemota:"+marcacao.getIpUnidadeRemota());
		System.out.println(" -> Tipo Dispositivo:"+marcacao.getTipoDispositivo());
		System.out.println(" -> Numero Leitora:"+marcacao.getNumeroDispositivo());
		System.out.println(" -> Codigo:"+marcacao.getCodigo());
		System.out.println(" -> Data/Hora:"+marcacao.getDataHora());
		System.out.println(" -> Ocorrencia:"+marcacao.getOcorrencia());

		EntityManager em = null;		
		Controladora controladora = null;
		try {
		
			switch (marcacao.getOcorrencia()) {

			case OCO_UR_ONLINE :				
				break;
			case OCO_UR_OFFLINE:
				
				System.out.println(" ---------------> in�cio update offline <--- " + marcacao.getIpUnidadeRemota());
				System.out.println(new Date().toString());

				em = new UnitOfWork().getEntityManager();
				controladora = Controladora.obterPorIP(marcacao.getIpUnidadeRemota(), em);
				
				if(controladora.isOnline()){
					em.getTransaction().begin();										
					controladora.setOnline(false);

					LogControladora logControladora = new LogControladora(controladora.getCodigo(),
							StatusLogControladora.Offline,
							"Informa��o obtida atrav�s do gerador de marca��o.", 
							controladora.getDataUltAtualizacao());

					em.persist(logControladora);
					
					em.getTransaction().commit();
				}
				
				System.out.println(" ---------------> fim update offline <--- " + marcacao.getIpUnidadeRemota());
				System.out.println(new Date().toString());
				
				break;
			default:								
				if(marcacao.getNumeroDispositivo() != 0){
					em = new UnitOfWork().getEntityManager();
					controladora = Controladora.obterPorIP(marcacao.getIpUnidadeRemota(), em);
					
					if(!controladora.isOnline()){
						System.out.println(" ---------------> teste: est� offline <--- " + marcacao.getIpUnidadeRemota());
						System.out.println(" ---------------> in�cio update online <--- " + marcacao.getIpUnidadeRemota());
						System.out.println(new Date().toString());
						em.getTransaction().begin();				
						controladora.setOnline(true);
						em.getTransaction().commit();
						System.out.println(" ---------------> fim update offline <--- " + marcacao.getIpUnidadeRemota());
						System.out.println(new Date().toString());
					}
					
					em.getTransaction().begin();
					
					/*Query query = em.createNativeQuery("INSERT INTO ACESSO_TEMP (TX_IP, NU_LEITORA, NU_OCORR, DT_HORA, NU_CRACHA, DT_REGISTRO) VALUES (?, ?, ?, ?, ?, ?)");
					
					query.setParameter(1, marcacao.getIpUnidadeRemota());
					query.setParameter(2, marcacao.getNumeroDispositivo());
					query.setParameter(3, marcacao.getOcorrencia());
					query.setParameter(4, marcacao.getDataHora());
					query.setParameter(5, marcacao.getCodigo());
					query.setParameter(6, new Date());
					
					query.executeUpdate();*/

					System.out.println(" ---------------------- sp_registra_acesso ---------------------- ");
					System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));
					
					StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_registra_acesso");

					sp.registerStoredProcedureParameter("ipUnidadeRemota", String.class, ParameterMode.IN);			 		
					sp.registerStoredProcedureParameter("numLeitora", int.class, ParameterMode.IN);
					sp.registerStoredProcedureParameter("ocorrencia", int.class, ParameterMode.IN);
					sp.registerStoredProcedureParameter("dtHora", Date.class, ParameterMode.IN);
					sp.registerStoredProcedureParameter("numeroCracha", String.class, ParameterMode.IN);			

					sp.setParameter("ipUnidadeRemota", marcacao.getIpUnidadeRemota());
					sp.setParameter("numLeitora", marcacao.getNumeroDispositivo());
					sp.setParameter("ocorrencia", marcacao.getOcorrencia());
					sp.setParameter("dtHora", marcacao.getDataHora());
					sp.setParameter("numeroCracha", marcacao.getCodigo());

					//sp.execute();

					Object[] a = null; 
					int xCodigoAcesso = 0;
					try{
						
						a = (Object[]) sp.getResultList().get(0);
						xCodigoAcesso = ((int)a[0]);
					}catch(Exception ex){
					}

					final int codigoAcesso = xCodigoAcesso;
					
					
					
					 //boolean veiculoPermissao = true;
			         //boolean papelPermissao = false;
			         
			         //Se o tipo da controladora for 3 � uma cancela
//			         if(controladora.getTipoControladora() == 3) {
//			        	 //Valida��o de ve�culo
//					 URL url = new URL("http://192.168.100.110:38880/api/integracao/ObterPermissaoVeiculo2?NumLeitora=" + marcacao.getNumeroDispositivo()  + "&IpControladora="+ controladora.getCodigo() +"&NumCracha=" + marcacao.getCodigo() + "idAcesso=" + xCodigoAcesso);
//		        	 //URL url = new URL("http//csadcda01v01.ternium.techint.net:38880http://192.168.100.110:38880/api/integracao/ObterPermissaoVeiculo?NumLeitora=" + marcacao.getNumeroDispositivo()  + "&IpControladora="+ controladora.getCodigo() +"&idCracha=" + marcacao.getCodigo() + "idAcesso=" + xCodigoAcesso);
//						   HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//						   conn.setRequestMethod("GET");
//						   conn.setRequestProperty("Accept", "application/json");
//					         //try {
//								
//							//} catch (ProtocolException e) {
//								// TODO Auto-generated catch block
//							//	e.printStackTrace();
//							//}
//					        
//					         //if (conn.getResponseCode() != 200) {
//					        //     throw new RuntimeException("Failed : HTTP Error code : "
//					         //            + conn.getResponseCode());
//					        // }
//					         InputStreamReader in = new InputStreamReader(conn.getInputStream());
//					         BufferedReader br = new BufferedReader(in);
//					         String output;
//					        
//					         while ((output = br.readLine()) != null) {
//					             //System.out.println(output);
//					        	
//					             System.out.println(veiculoPermissao);
//					         }
//					         
//					         conn.disconnect();    
//					     
//					      
//					         
//					         //Verifica��o de se o ve�culo e o Papel tem permiss�o de acesso com c�meras ligadas        
//					         if(output.equalsIgnoreCase("\"true\"") || output.equalsIgnoreCase("{true}")){
//				        		 veiculoPermissao = true;
//				        		//Verifica��o de se o ve�culo e o Papel tem permiss�o de acesso com c�meras desligadas    
//				        	 }else {
//				        		 veiculoPermissao = false;
//				        	 }
//					        //}
					if(codigoAcesso > 0){
						System.out.println(" ---------------------- Identificado sob analise ---------------------- ");
						System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));
						
						
						
						
						new Thread(new Runnable() {
						    public void run() {
								System.out.println(" ---------------------- executePost ---------------------- ");
								System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));
								String endereco = getPropValues("AppWebEndereco");
								String porta = getPropValues("AppWebPorta");
								String urlAddress = "http://" + endereco + ":" + porta + "/MonitorarAcessos/EnviarEmail";
								String urlParameters = "codigo=" + codigoAcesso; //URLEncoder.encode("00000000000", "UTF-8");
								String responseMail = AcessoDB.executePost(urlAddress, urlParameters);
								System.out.println(responseMail);
							}
						}).start();
					}
					
					em.getTransaction().commit();

					try {
						if(controladora.getTipoControladora() == 3 && controladora.getValidaOCR() == true) {
						EntityManagerFactory emFactory = null;
						emFactory = Persistence.createEntityManagerFactory("conexao");	
						EntityManager emApi = null;		
						emApi = emFactory.createEntityManager();
						List<String> params = new LinkedList<String>();
						params.add("IP_INTEGRADOR");
						params.add("PORTA_INTEGRADOR_AVIGILON");
						List<Parametro> parametros = Parametro.obterParametros(params, emApi);
						
						String IP_INTEGRADOR = String.valueOf(obterParametro(parametros, "IP_INTEGRADOR"));
						String PORTA_INTEGRADOR_AVIGILON = String.valueOf(obterParametro(parametros, "PORTA_INTEGRADOR_AVIGILON"));
					
						//String ATIVACAO_CONSULTA_INTEGRADOR = String.valueOf(obterParametro(parametros, "ATIVACAO_CONSULTA_INTEGRADOR"));
						
						
						//String urlAtualiza =  "http://localhost:62760/api/integracao/AtualizarACessoVeiculo?idAcesso=" + codigoAcesso  + "&numeroCracha="+   marcacao.getCodigo();
						 String urlAtualiza =  IP_INTEGRADOR + ":"+ PORTA_INTEGRADOR_AVIGILON + "/api/integracao/AtualizarACessoVeiculo?idAcesso=" + xCodigoAcesso  + "&numeroCracha="+  marcacao.getCodigo();
						  URL url = new URL(urlAtualiza);
					         //URL url = new URL(IP_INTEGRADOR + ":" + PORTA_INTEGRADOR_AVIGILON + "/api/integracao/ObterPermissaoVeiculo?IdLeitora=" + leitora.getCodigo()  + "&IpControladora="+ controladora.getCodigo() +"&numCracha=" + solicitacaoDTO.getCodigo() + "&papelPermissao=" + temPermissao);
								    //URL url = new URL("http://192.168.100.110:38880/api/integracao/ObterPermissaoVeiculo?IdLeitora=" + 57  + "&IpControladora="+ 106 +"&numCracha=" + 3543793+ "&papelPermissao=" + temPermissao);
						  	String output = "";
						   HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						   
						   conn.setRequestMethod("GET");
						   conn.setRequestProperty("Accept", "application/json");
					       
					         InputStreamReader in = new InputStreamReader(conn.getInputStream());
					         BufferedReader br = new BufferedReader(in);
					        
								   while ((output = br.readLine()) != null) {
									  System.out.println(" ---------------------- sp_registra_acesso ---------------------- ");			            
							         }
			 			      
			 				   conn.disconnect();
						}
					}catch(Exception ExAvig) {
						
					}
					
				}
				break;
			}
		}catch(Exception ex){
			if(em != null && em.getTransaction().isActive())
				em.getTransaction().rollback();

			ex.printStackTrace();					
		}finally{
			if(em != null) {
				//em.close();
				em.getEntityManagerFactory().close();						
			}
		}

		System.out.println(" ---------------------- Fim Trata Marcacao ---------------------- ");
		System.out.println(new SimpleDateFormat("yyyy-MM-dd EEE HH:mm:ss.SSS").format(new Date()));
	}

	public void indicadorHardware(int concentrador, String ipUnidadeRemota, IndicadorHardwareDTO indicadorHardware) {
		System.out.println(" ---------------------- Indicadores Hardware ---------------------- ");
		System.out.println(" -> Concentrador: "+concentrador);
		System.out.println(" -> Ip UnidadeRemota: "+ipUnidadeRemota);
		System.out.println(" -> Tensao: "+indicadorHardware.getTensao()+" V");
		System.out.println(" -> Temperatura: "+indicadorHardware.getTemperatura()+"�C");
		System.out.println(" -> Corrente: "+indicadorHardware.getCorrente()+"mA");
		System.out.println(" -> Versao Firmware: "+indicadorHardware.getVersaoFirmware());
		System.out.println(" -> Numero Serial: "+indicadorHardware.getNumeroSerial());
		System.out.println(" -> Total lista ordenada: "+indicadorHardware.getQtdUsuariosListaOrdenada());
		System.out.println(" -> Total max lista ordenada: "+indicadorHardware.getQtdMaxUsuariosListaOrdenada());
		System.out.println(" -> Total lista nao ordenada: "+indicadorHardware.getQtdUsuariosListaDesordenada());
		System.out.println(" -> Total max lista nao ordenada: "+indicadorHardware.getQtdMaxUsuariosListaDesordenada());

		EntityManager em = null;		

		try {
			em = new UnitOfWork().getEntityManager();       

			System.out.println(" ---------------------- In�cio setando online ip ---------------------- " + ipUnidadeRemota);
			System.out.println(new Date().toString());
			Controladora controladora = Controladora.obterPorIP(ipUnidadeRemota, em);

			if(!controladora.isOnline()){
				em.getTransaction().begin();
				controladora.setOnline(true);
				controladora.setTensao(indicadorHardware.getTensao());
				controladora.setTemperatura(indicadorHardware.getTemperatura());
				controladora.setCorrente(indicadorHardware.getCorrente());
				controladora.setVersaoFirmWare(indicadorHardware.getVersaoFirmware());
				controladora.setNumeroSerial(indicadorHardware.getNumeroSerial());
				controladora.setQtdListaOrdenada(indicadorHardware.getQtdUsuariosListaOrdenada());
				controladora.setQtdMaxListaOrdenada(indicadorHardware.getQtdMaxUsuariosListaOrdenada());
				controladora.setQtdMaxListaDesordenada(indicadorHardware.getQtdMaxUsuariosListaDesordenada());
				controladora.setQtdListaDesordenada(indicadorHardware.getQtdUsuariosListaDesordenada());
				controladora.setDataUltAtualizacao(new Date());

				LogControladora logControladora = new LogControladora(controladora.getCodigo(),
						StatusLogControladora.Online,
						"Informa��o obtida atrav�s do indicador de hardware.", 
						controladora.getDataUltAtualizacao());
				em.persist(logControladora);

				em.getTransaction().commit();
			}

			System.out.println(" ---------------------- Fim setando online ip ---------------------- " + ipUnidadeRemota);
			System.out.println(new Date().toString());

		}catch(Exception ex){
			if(em.getTransaction().isActive())
				em.getTransaction().rollback();

			ex.printStackTrace();					
		}finally{
			if(em != null) {
				//em.close();
				em.getEntityManagerFactory().close();						
			}
		}
	}

	public void atualizaBiometria(String idUsuario, int tipoBiometria, int idBiometria, String biometria) {
		System.out.println(" ---------------------- Indicadores Hardware ---------------------- ");
		System.out.println(" -> idUsuario:" + idUsuario);
		System.out.println(" -> tipoBiometria:" + tipoBiometria);
		System.out.println(" -> idBiometria:"+idBiometria);
		System.out.println(" -> biometria:"+biometria);
	}

	public void atualizaStatusAlarme(int idAlarme, int zonaAlarme, boolean habilitado) {
		System.out.println(" ---------------------- Atualiza Status Alarme ---------------------- ");
		System.out.println(" -> idAlarme:"+idAlarme);
		System.out.println(" -> zonaAlarme:"+zonaAlarme);
		System.out.println(" -> habilitado:"+habilitado);
	}

	private UnidadeRemotaDTO obterUnidadeRemota(int portaEntrada, int tempoEsperaInput2,
			int tempoRepeteInput1, int intervaloRepeteInput1, int tempoGiro,
			int intervaloKeepOn, int intervaloKeepOff,
			Controladora controladora) {

		List<Leitora> leitoras = controladora.obterLeitoras(true);				

		int[] leitoraTipo = {0, 0, 0, 0};
		int[] tecladoTipo = {0, 0, 0, 0};
		char[] leitoraSentido = {'E', 'S', 'E', 'S'};
		char[] tecladoSentido   = {'E', 'S', 'E', 'S'};					

		int i = 0;

		while(i < leitoras.size() && i < 4)
		{						
			Leitora leitora = leitoras.get(i);

			leitoraTipo[i] = leitora.getFabricante();
			leitoraSentido[i] = leitora.getDirecaoLeitora().charAt(0);						

			i++;
		}

		int[] biometriaTipo     = {0,0};
		int[] handkeyTipo       = {0,0};		

		int[] listaDigitoInical = {1,1,1,1};
		int[] listaDigitoFinal  = {14,14,14,14};		

		UnidadeRemotaDTO temp = new UnidadeRemotaDTO();						

		/** Configuracao padrao */
		temp.setModelo("MDC4IP");
		temp.setIp(controladora.getIp());
		temp.setPortaEntrada(portaEntrada);
		temp.setTempoEsperaInput2(tempoEsperaInput2);
		temp.setTempoRepeteInput1(tempoRepeteInput1);
		temp.setIntervaloRepeteInput1(intervaloRepeteInput1);
		temp.setTempoGiro(tempoGiro); //segundos
		temp.setModoAcionamentoPadrao(1);  //Criar modo acionamento padr�o
		temp.setIntervaloKeepOn(intervaloKeepOn);
		temp.setIntervaloKeepOff(intervaloKeepOff);
		temp.setIntervaloMonitoramentoHardware(300); //pIndicadorHardware 

		/** Configuracao leitoras */
		temp.setTipoUnidadeRemota(controladora.getTipoControladora());
		temp.setLeitoraTipo(leitoraTipo);
		temp.setLeitoraSentido(leitoraSentido);
		temp.setTecladoTipo(tecladoTipo);
		temp.setTecladoSentido(tecladoSentido);
		temp.setBiometriaTipo(biometriaTipo);
		temp.setHandkeyTipo(handkeyTipo);

		/** Configuracao da lista */
		temp.setListaDigitoInical(listaDigitoInical);
		temp.setListaDigitoFinal(listaDigitoFinal);
		temp.setListaHabilitada(true); //habilita ou desabilita a consulta na unidade remota
		temp.setListaTipo(1); //habilita ou desabilita a consulta na unidade remota
		temp.setListaZonaHabilitada(true);

		/** Sensores */
		boolean[] sensorLeitoras = {false,false,false,false};
		boolean[] sensorTeclados = {false,false,false,false};

		boolean[] sensorLeitoras1 = {true,true,false,false};
		boolean[] sensorTeclados1 = {false,false,false,false};

		int tempTimeoutPortaAberta = 100;
		int estadoAcionamentoSensor = 0;

		temp.setSensorDTO(0,new SensorDTO(sensorLeitoras,sensorTeclados,0,estadoAcionamentoSensor,154,true));
		temp.setSensorDTO(1,new SensorDTO(sensorLeitoras1,sensorTeclados1,tempTimeoutPortaAberta,1,200,true));                                  
		temp.setSensorDTO(2,null);
		temp.setSensorDTO(3,null);
		temp.setSensorDTO(4,null);

		return temp;
	}

	public static String executePost(String targetURL, String urlParameters) {
		
		  HttpURLConnection connection = null;

		  try {
		    //Create connection
		    URL url = new URL(targetURL);
		    connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("POST");
		    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

		    connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
		    connection.setRequestProperty("Content-Language", "en-US");  

		    connection.setUseCaches(false);
		    connection.setDoOutput(true);

		    //Send request
		    DataOutputStream wr = new DataOutputStream (
		        connection.getOutputStream());
		    wr.writeBytes(urlParameters);
		    wr.close();
		    
		    String response = "";
	        Reader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        	for (int c; (c = in.read()) >= 0;)
        		response += ((char)c);
        	
		    return response.toString();
		  } catch (Exception e) {
		    e.printStackTrace();
		    return null;
		  } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
		  }
		  
	}

	/*private Vector<RegistroDTO> obterFaixasHorarias(List<EscalaTrabalho> escalas) {
		List<HoraTurno> horasTurno = EscalaTrabalho.obterHorasTurno(escalas);

		Vector<RegistroDTO> faixasHorarias = new Vector<RegistroDTO>();														

		for (HoraTurno horaTurno : horasTurno){

			String horaInicial = String.format("%02d%02d", horaTurno.getHoraInicio().get(Calendar.HOUR_OF_DAY), horaTurno.getHoraInicio().get(Calendar.MINUTE)); 
			String horaFinal = String.format("%02d%02d", horaTurno.getHoraFim().get(Calendar.HOUR_OF_DAY), horaTurno.getHoraFim().get(Calendar.MINUTE));																	

			faixasHorarias.add(new FaixaHorariaDTO(horaTurno.getCodigo(), horaInicial, horaFinal));
		}

		return faixasHorarias;
	}

	private Vector<RegistroDTO> obterZonasTempo(List<EscalaTrabalho> escalas) {

		List<GrupoTrabalho> grupos = EscalaTrabalho.obterGruposTrabalho(escalas);

		Vector<RegistroDTO> registros = new Vector<RegistroDTO>();

		for (GrupoTrabalho grupoTrabalho : grupos) {										

			int[] domingo = new int[4];
			int[] segunda = new int[4];
			int[] terca = new int[4];
			int[] quarta = new int[4];
			int[] quinta = new int[4];
			int[] sexta = new int[4];
			int[] sabado = new int[4];

			List<EscalaTrabalho> escalasGrupo = EscalaTrabalho.obterEscalasPorGrupo(escalas, grupoTrabalho);

			for (EscalaTrabalho escalaTrabalho : escalasGrupo) {

				HoraTurno horaTurno = escalaTrabalho.getHoraTurno();													

				Calendar c = Calendar.getInstance();
				c.setTime(escalaTrabalho.getDataEscala());

				switch (c.get(Calendar.DAY_OF_WEEK)){
				case Calendar.SUNDAY:
					domingo[0] = horaTurno.getCodigo();
					domingo[1] = horaTurno.getCodigo();
					break;

				case Calendar.MONDAY:
					segunda[0] = horaTurno.getCodigo();
					segunda[1] = horaTurno.getCodigo();
					break;

				case Calendar.TUESDAY:
					terca[0] = horaTurno.getCodigo();
					terca[1] = horaTurno.getCodigo();
					break;

				case Calendar.WEDNESDAY:
					quarta[0] = horaTurno.getCodigo();
					quarta[1] = horaTurno.getCodigo();
					break;

				case Calendar.THURSDAY:
					quinta[0] = horaTurno.getCodigo();
					quinta[1] = horaTurno.getCodigo();
					break;

				case Calendar.FRIDAY:
					sexta[0] = horaTurno.getCodigo();
					sexta[1] = horaTurno.getCodigo();
					break;

				case Calendar.SATURDAY:
					sabado[0] = horaTurno.getCodigo();
					sabado[1] = horaTurno.getCodigo();
					break;								
				}
			}

			registros.add(new ZonaTempoDTO(grupoTrabalho.getCodigo(),domingo,segunda,terca,quarta,quinta,sexta,sabado));
		}

		return registros;
	}*/

	private Vector<RegistroDTO> obterConfiguracoesDeInput(Controladora controladora) {

		Vector<RegistroDTO> registros = new Vector<RegistroDTO>();

		boolean[] primeiroInput     = {true,true,true,true,false,false,false,false,false,false,false,false,false,false,false,false};
		boolean[] segundoInputL1    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		boolean[] segundoInputL2    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		boolean[] segundoInputL3    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		boolean[] segundoInputL4    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};

		List<Leitora> leitoras = controladora.obterLeitoras(true);					

		boolean[] segundoInputT1    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		boolean[] segundoInputT2    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};							
		boolean[] segundoInputT3    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		boolean[] segundoInputT4    = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};

		if(leitoras.size() >= 1 && leitoras.get(0).isSenha()){
			segundoInputT1[0] = true;
			segundoInputT1[1] = true;
			segundoInputT1[2] = true;
			segundoInputT1[3] = true;	
		}

		if(leitoras.size() >= 2 && leitoras.get(1).isSenha()){
			segundoInputT2[0] = true;
			segundoInputT2[1] = true;
			segundoInputT2[2] = true;
			segundoInputT2[3] = true;
		}

		if(leitoras.size() >= 3 && leitoras.get(2).isSenha()){
			segundoInputT3[0] = true;
			segundoInputT3[1] = true;
			segundoInputT3[2] = true;
			segundoInputT3[3] = true;
		}

		if(leitoras.size() >= 4 && leitoras.get(3).isSenha()){
			segundoInputT4[0] = true;
			segundoInputT4[1] = true;
			segundoInputT4[2] = true;
			segundoInputT4[3] = true;
		}

		registros.add(new InputHabilitadoDTO(1,primeiroInput,segundoInputL1,segundoInputL2,segundoInputL3,segundoInputL4,segundoInputT1,segundoInputT2,segundoInputT3,segundoInputT4));

		return registros;
	}

	private Vector<RegistroDTO> obterConfiguracaoAcionamentos() {
		Vector<RegistroDTO> registros = new Vector<RegistroDTO>();

		int[] tempoLed1          = { 10,0,0,0 };
		int[] tempoBuzzer1       = { 10,0,0,0 };
		int[] tempoRele1         = { 10,0,0,0 };
		int[] tempoSaida1        = { 0,0,0,0,0,0,0,0 };
		int id_display1          = 1;
		int id_mensagem1         = 1;
		registros.add(new AcionamentoDTO(1,tempoLed1,tempoBuzzer1,tempoRele1,tempoSaida1,id_display1,id_mensagem1));

		int[] tempoLed2          = { 0,10,0,0 };
		int[] tempoBuzzer2       = { 10,0,0,0 };
		int[] tempoRele2         = { 0,0,0,0 };
		int[] tempoSaida2        = { 0,0,0,0,0,0,0,0 };
		int id_display2          = 1;
		int id_mensagem2         = 2;
		registros.add(new AcionamentoDTO(2,tempoLed2,tempoBuzzer2,tempoRele2,tempoSaida2,id_display2,id_mensagem2));													

		int[] tempoLed3          = { 0,10,0,0 };
		int[] tempoBuzzer3       = { 10,0,0,0 };
		int[] tempoRele3         = { 10,10,10,10 };
		int[] tempoSaida3        = { 0,0,0,0,0,0,0,0 };
		int id_display3          = 0;
		int id_mensagem3         = 2;		            
		registros.add(new AcionamentoDTO(154,tempoLed3,tempoBuzzer3,tempoRele3,tempoSaida3,id_display3,id_mensagem3));

		return registros;
	}

	private Vector<RegistroDTO> obterModosAcionamentos() {
		Vector<RegistroDTO> registros = new Vector<RegistroDTO>();

		int[] leitora1          = { 1,1,1,2 };
		int[] leitora2          = { 1,1,1,2 };
		int[] leitora3          = { 1,1,1,2 };
		int[] leitora4          = { 1,1,1,2 };
		int[] teclado1          = { 1,1,1,2 };
		int[] teclado2          = { 1,1,1,2 };
		int[] teclado3          = { 0,0,0,0 };
		int[] teclado4          = { 0,0,0,0 };

		registros.add(new ModoAcionamentoDTO(1, leitora1, leitora2, leitora3, leitora4, teclado1, teclado2, teclado3, teclado4));

		return registros;
	}

	private String obterParametro(List<Parametro> parametros, String name) {
		for (Parametro parametro : parametros) {
			if (parametro.getNome().equals(name))
				return parametro.getValor();			
		}

		return "";
	}

	private Vector<RegistroDTO> obterUsuarios(Controladora controladora, Date dataLimite, EntityManager em) {
		Vector<RegistroDTO> registros = new Vector<RegistroDTO>();

		List<Pessoa> pessoas = Pessoa.obterPessoas(controladora.getCodigo(), dataLimite, em);

		for (Pessoa pessoa : pessoas)							
			registros.add(criarUsuario(pessoa));		

		return registros;
	}

	private UsuarioDTO criarUsuario(Pessoa pessoa) {
		boolean habilitado;
		String input1;
		String input2;
		boolean usaPassBack;
		boolean usaZonaTempo;
		int idModoLiberacao;
		int idInputHabilitado;
		int idZonaTempo;

		/******** Cart�o ********/
		habilitado       	= true;
		input1           	= pessoa.getNumCracha();								
		input2           	= "00000000000000";				
		usaPassBack     	= false;
		usaZonaTempo 		= false;
		idModoLiberacao     = 1;					
		idZonaTempo 		= 0;

		if(pessoa.getSenha() != null)
			input2           	= pessoa.getSenha();			

		/*if(pessoa.getGrupoTrabalho() != null) {
			usaZonaTempo = true;
			idZonaTempo = pessoa.getGrupoTrabalho().getCodigo();
		}*/

		idInputHabilitado   = 1;			

		return new UsuarioDTO(habilitado,input1,input2,usaPassBack,usaZonaTempo,idModoLiberacao,idInputHabilitado,idZonaTempo);
	}

	private Date convertDate(String valor) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");								
		Date data = new Date(); 

		if(valor != null)
			data = formatter.parse(valor);

		return data;		
	}
	
	private String getPropValues(String parametro) {
		
		String response = null;
		
		try{

			if(parametro == null || parametro == "")
				Log.gravaLogErros(getClass().getPackage().getName(), "Parametro nulo ou invalido no arquivo de configuracao", "");
			
			Properties props = new Properties();
	        String args = AcessoDB.class.getResource("/META-INF/config.props").getFile();
			props.load(new FileInputStream(args));
	
			response = props.getProperty(parametro);
			if (response == null)
		        Log.gravaLogErros(getClass().getPackage().getName(), "Nao foi possivel localizar o " + parametro +" no arquivo de configuracao", "");
			
		}
	    catch (Exception e)
	    {
	      System.err.println("Nao foi possivel carregar propriedades do arquivo: " + e.toString());
	    }

		return response;
			
	}
}
